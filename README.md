# Pokemon React App
## Getting Started

First, install the dependencies before running the development server:

```bash
yarn
#or
npm install
```

```bash
npm run dev
# or
yarn dev
```

Access project on the browser via [http://localhost:3000](http://localhost:3000).

## Login Credentials

Here is the static credential for the login form:

```
email: admin@email.com
password: admin
```

## Linting

I've included ESlint in respect with React and TypeScript defaults with a few tweaks. You can run the linter via `yarn eslint:check`.
