import { ChangeEvent, FC, FormEvent, useState } from 'react'
import { Box, FormControl, Text, Input, Button, Stack } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { LoginProps } from '../../lib/types'

const LoginForm: FC = () => {
  const admin = {
    email: 'admin@email.com',
    password: 'admin'
  }

  const errorMessage = "Invalid credentials. Please check your input carefully or see if caps lock is on."

  const router = useRouter()

  const [loginData, setLoginData] = useState<LoginProps>({ email: '', password: '' })
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [error, setError] = useState<string>("")
  

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setLoginData({...loginData, [event.target.name]: event.target.value})
  }

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    login(loginData)
  }

  const handleValidation = (error) =>{
    setError(error)
  }

  const login = (credentials: LoginProps) => {
    const { email, password } = credentials

    if (email === admin.email && password === admin.password) {
      // Setting timeout to simulate load state before logging in
      setIsLoading(true)
      
      setTimeout(() => {
        router.push('/dashboard')
      }, 2000)
    } else {
      handleValidation(errorMessage)
    }
  }

  return (
    <Box 
      bg='gray.700' 
      w={{ base: "290px", md: "lg", lg: "lg" }} 
      borderRadius="lg" 
      overflow="hidden" 
      px={{ base: 8, md: 12, lg: 12 }} 
      py={{base: 16, md: 32, lg: 32}}
    >
      <form onSubmit={handleSubmit}>
        <Stack spacing={5} display="">
          <FormControl id="email">
            <Input 
              type="email" 
              name="email" 
              bg="gray.600" 
              placeholder="Email" 
              h={12} 
              _focus={{ borderColor: '#F2C94C' }} 
              onChange={handleChange}
            />
          </FormControl>
          <FormControl id="password">
            <Input 
              type="password" 
              name="password" 
              bg="gray.600" 
              placeholder="Password" 
              h={12} 
              _focus={{ borderColor: '#F2C94C' }} 
              onChange={handleChange}
            />
          </FormControl>
        </Stack>
        {(error != "") ? (<Text color="red.600" maxW="416px" mt={6}>{error}</Text>) : ""}
        <Button type="submit" bg="yellow.400" h={12} mt={10} w="100%" textTransform="uppercase" isLoading={isLoading}>Login</Button>
      </form>
    </Box>
  )
}

export default LoginForm
