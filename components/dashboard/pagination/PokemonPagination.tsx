
import { FC } from 'react'
import { Box, Stack } from '@chakra-ui/react'

import PaginationControl from './PaginationControl'

type PaginationProps = {
  current: number
  onChange(page: number): void
  hasControls: boolean
}

export const PokemonPagination: FC<PaginationProps> = ({ current, onChange, hasControls }) => {
  const increment = () => onChange(current + 1)
  const decrement = () => onChange(current - 1)
  const pageSet = (num: number) => () => onChange(num)

  return(
    <>
      <Box 
        p={5} 
        backgroundColor="gray.900" 
        position="absolute" 
        top={{ base: 0, md: "700px" , lg: "calc(680px - 80px)"}}
        width="100%" 
        borderBottomLeftRadius="lg"
      >
        <Stack direction="row">
          {hasControls && (
            <PaginationControl value={current + 1} onIncrement={increment} onDecrement={decrement} onSetPrev={pageSet(current - 1)} onSet={pageSet(current + 1)} />
          )}
        </Stack>
      </Box>
    </>
  )
}

export default PokemonPagination
