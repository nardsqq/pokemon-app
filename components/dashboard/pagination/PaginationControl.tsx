
import { FC } from 'react'
import { Button, Stack } from '@chakra-ui/react'

type PaginationControlProps = {
  value: number
  onIncrement(): void
  onDecrement(): void
  onSetPrev(): void
  onSet(): void
}

export const PaginationControl: FC<PaginationControlProps> = ({ value, onIncrement, onDecrement, onSetPrev, onSet }) => {
  return(
    <>
      <Stack direction="row" spacing={52}>
        <Stack direction="row">
          <Button backgroundColor="gray.700" width="16px" onClick={onSetPrev} disabled={(value - 1) === 1 ? true : false}>
            {value - 1}
          </Button>
          <Button backgroundColor="gray.700" width="16px" onClick={onSet} disabled={value === 16 ? true : false}>
            {value}
          </Button>
        </Stack>
        <Stack direction="row">
          <Button backgroundColor="gray.700" width="72px" onClick={onDecrement} disabled={(value - 1) === 1 ? true : false}>
            Prev
          </Button>
          <Button backgroundColor="gray.700" width="72px" onClick={onIncrement} disabled={value === 16 ? true : false}>
            Next
          </Button>
        </Stack>
      </Stack>
    </>
  )
}

export default PaginationControl
