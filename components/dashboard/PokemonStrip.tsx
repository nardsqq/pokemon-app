import { Avatar, Text, HStack, Button } from '@chakra-ui/react'
import { PokemonStripProps } from '../../lib/types'

const PokemonStrip = ({ pokemons, clickHandler }: PokemonStripProps) => {
  return (
    <>
      {pokemons && pokemons.map(pokemon => {
        const {id, name, image, number} = pokemon
        
        return (
          <Button 
            key={id} 
            id={id} 
            px={16}
            py={12} 
            w={{ base: "240px", md: "sm", lg: "sm" }}
            backgroundColor="gray.600" 
            borderRadius="lg" 
            justifyContent={{ base: "center", md: "flex-start", lg: "flex-start" }}
            onClick={clickHandler}
          >
            <HStack spacing={6}>
              <Avatar name={name} src={image}/>
              <HStack>
                <Text color="yellow.400">{number}</Text>
                <Text>{name}</Text>
              </HStack>
            </HStack>
          </Button>
        )
      })}
    </>
  )
}

export default PokemonStrip
