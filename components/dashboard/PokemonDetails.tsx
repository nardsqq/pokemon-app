import { Box, Flex, Heading, Spacer } from '@chakra-ui/react'
import { useState, useEffect } from 'react'
import { PokemonDetailsProps } from '../../lib/types'

import PokemonCard from '../dashboard/PokemonCard'

const PokemonDetails = ({ pokemons, selected }: PokemonDetailsProps) => {
  const [pokemonDetail, setPokemonDetail] = useState({ name: '', number: '' })

  useEffect(() => {
    setPokemonDetail(pokemons?.find(pokemon => pokemon.id === selected))
  }, [selected])
  
  return(
    <Box
      backgroundColor="gray.500" 
      width={{ base: "300px", md: "calc(960px - 380px)", lg: "calc(960px - 380px)" }}
      height={{base: "720px", md: "780px", lg: "680px" }}
      borderTopRightRadius="lg"
      borderTopLeftRadius={{ base: "lg", md: "none", lg: "none" }}
      borderBottomRightRadius="lg"
      borderBottomLeftRadius={{ base: "lg", md: "none", lg: "none" }}
    >
      <Flex p={8} borderBottom="2px solid" borderBottomColor="gray.700" mb={6}>
        <Heading as="h2" fontWeight="regular">{pokemonDetail && pokemonDetail.name}</Heading>
        <Spacer />
        <Heading as="h2" fontWeight="regular" color="yellow.400">{pokemonDetail && pokemonDetail.number}</Heading>
      </Flex>
      <PokemonCard pokemonDetail={pokemonDetail}/>
    </Box>
  )
}

export default PokemonDetails
