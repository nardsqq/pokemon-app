import { Fragment } from 'react'

import { 
  Avatar, 
  Stack, 
  Text, 
  Box, 
  Badge, 
  Spacer, 
  Divider, 
  Stat, 
  StatLabel, 
  StatNumber, 
  StatHelpText, 
  StatArrow } from '@chakra-ui/react'

import { PokemonCardProps } from '../../lib/types'

const PokemonCard = ({ pokemonDetail }: PokemonCardProps) => {
  return (
    <Box p={8}>
      <Stack direction="row" spacing={4} align="center">
        <Avatar
          src={pokemonDetail?.image}
          alt={pokemonDetail?.name}
        />
        <Stack direction="column" spacing={0} fontSize="sm">
          <Text fontWeight={600}>{pokemonDetail?.name}</Text>
          <Text color="yellow.400">{pokemonDetail?.classification}</Text>
        </Stack>
        <Spacer />
        <Stack>
          {pokemonDetail?.types?.map((type, i) => {
            return (
              <Badge borderRadius="full" fontSize={{ base: "xs", md: "xx-small", lg: "xs" }} px="2" mx="1" colorScheme="yellow" key={i}>{type}</Badge>
            )
          })}
        </Stack>
      </Stack>
      <Divider mt={6} />
      <Stack mt={6} direction="row" spacing={4} align="center">
        <Stack direction="column" spacing={1} fontSize="sm">
          <Stat>
            <StatLabel>Height</StatLabel>
            <StatNumber color="yellow.400">{pokemonDetail?.height?.maximum}</StatNumber>
            <StatHelpText>{pokemonDetail?.height?.minimum} &#8212; {pokemonDetail?.height?.maximum}</StatHelpText>
          </Stat>
          <Stat>
            <StatLabel>Weight</StatLabel>
            <StatNumber color="yellow.400">{pokemonDetail?.weight?.maximum}</StatNumber>
            <StatHelpText>{pokemonDetail?.height?.minimum} &#8212; {pokemonDetail?.height?.maximum}</StatHelpText>
          </Stat>
        </Stack>
        <Spacer />
        <Stat>
          <StatLabel fontSize={{ base: "xs", md: "sm", lg: "sm" }}>Max HP</StatLabel>
          <StatNumber fontSize={{ base: "lg", md: "lg", lg: "2xl" }} color="green.500">{pokemonDetail?.maxHP}</StatNumber>
          <StatHelpText display={{ base: "none", md: "none", lg: "block" }}>
            <StatArrow type="increase" />
            HP &#37;
          </StatHelpText>
        </Stat>
        <Stat>
          <StatLabel fontSize={{ base: "xs", md: "sm", lg: "sm" }}>Max CP</StatLabel>
          <StatNumber fontSize={{ base: "lg", md: "lg", lg: "2xl" }} color="green.500">{pokemonDetail?.maxCP}</StatNumber>
          <StatHelpText display={{ base: "none", md: "none", lg: "block" }}>
            <StatArrow type="increase" />
            CP &#37;
          </StatHelpText>
        </Stat>
      </Stack>
      <Divider my={6} />
      <Stack direction="column" spacing={1} fontSize="sm">
        <Stat>
          <StatLabel my={2}>Resistances</StatLabel>
          <StatHelpText>
            {pokemonDetail?.resistant?.map((resistance, i) => {
              return(
                <Fragment key={i}>
                  <StatArrow type="increase" mx={2}/>
                  {resistance}
                </Fragment>
              )
            })}
          </StatHelpText>
        </Stat>
        <Stat>
          <StatLabel my={2}>Weaknesses</StatLabel>
          <StatHelpText>
            {pokemonDetail?.weaknesses?.map((weakness, i) => {
              return(
                <Fragment key={i}>
                  <StatArrow type="decrease" mx={2}/>
                  {weakness}
                </Fragment>
              )
            })}
          </StatHelpText>
        </Stat>
      </Stack>
    </Box>
  )
}

export default PokemonCard
