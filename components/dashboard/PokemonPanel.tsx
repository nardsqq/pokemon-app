import { useState, MouseEvent } from 'react'
import { Box, HStack, VStack } from '@chakra-ui/react'

import PokemonStrip from './PokemonStrip'
import PokemonDetails from './PokemonDetails'
import PokemonPagination from './pagination/PokemonPagination'

import { Pokemons } from '../../lib/types'

const PokemonPanel = ({ pokemons }: Pokemons) => {
  const [page, setPage] = useState(0)

  const pokemonPerPage = 10
  const currentPage = page + 1
  const indexOfLastPokemon = currentPage * pokemonPerPage
  const indexOfFirstPokemon = indexOfLastPokemon - pokemonPerPage
  const pokemonsOnPage = pokemons.slice(indexOfFirstPokemon, indexOfLastPokemon)

  const [selectedPokemon, setSelectedPokemon] = useState<string>('')

  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    setSelectedPokemon(event.currentTarget.id)
  }

  const handlePageChange = (count: number) => {
    setPage(count - 1)
  }

  return (
    <>
      <Box display={{ base: "none", md: "block", lg: "block" }} w={{ md: "768px", lg: "980px" }}>
        <HStack spacing={0}>
          <VStack spacing={0} position="relative">
            <Box 
              backgroundColor="gray.700" 
              p={14} 
              borderTopLeftRadius="lg" 
              borderBottomLeftRadius="lg" 
              overflowY="scroll" 
              css={{ '&::-webkit-scrollbar': { display: 'none' } }}
              maxH={{ md: "780px" , lg: "680px" }}
            >
              <VStack spacing={4}>
                <PokemonStrip pokemons={pokemonsOnPage} clickHandler={handleClick} />
              </VStack>
            </Box>
            <PokemonPagination current={page + 1} onChange={handlePageChange} hasControls={true}/>
          </VStack>
            <PokemonDetails pokemons={pokemons} selected={selectedPokemon} />
        </HStack>
      </Box>
      
      <Box display={{ base: "flex", md: "none", lg: "none" }} flexDirection="column">
        <VStack>
          <Box
            backgroundColor="gray.700"
            borderRadius="lg" 
            overflowY="scroll" 
            overflowX="hidden"
            css={{ '&::-webkit-scrollbar': { display: 'none' } }} 
            h="280px"
            w="300px"
            marginTop="260px"
            marginBottom="24px"
          >
            <VStack spacing={4} paddingY={6}>
              <PokemonStrip pokemons={pokemons} clickHandler={handleClick} />
            </VStack>
          </Box>
        </VStack>
        <Box marginBottom="24px">
          <PokemonDetails pokemons={pokemons} selected={selectedPokemon} />
        </Box>
      </Box>
    </>
  )
}

export default PokemonPanel
