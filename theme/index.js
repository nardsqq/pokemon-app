import { extendTheme } from '@chakra-ui/react'

const config = {
  initialColorMode: "dark",
  useSystemColorMode: false,
}

const overrides = {
  colors: {
    gray: {
      400: '#484D57',
      500: '#3B3E46',
      600: '#3F414B',
      700: '#2D2F36',
      800: '#1C1D1F',
      900: '#1F1F1F'
    },
  }
}

const theme = extendTheme({config}, overrides)

export default theme
