import { MouseEvent } from 'react'
import { InferGetStaticPropsType } from 'next'
import { getStaticProps } from '../../pages/dashboard'

export type Pokemon = {
  id?: string
  number: string
  name: string
  image?: string
  classification?: string
  maxCP?: number
  maxHP?: number
  resistant?: string[]
  weaknesses?: string[]
  types?: string[]
  weight?: { minimum: string, maximum: string }
  height?: { minimum: string, maximum: string }
}

export type Pokemons = {
  pokemons: [Pokemon]
}

export type PokemonStripProps = {
  pokemons: InferGetStaticPropsType<typeof getStaticProps>
  clickHandler: (event: MouseEvent<HTMLButtonElement>) => void;
}

export type PokemonDetailsProps = {
  pokemons: InferGetStaticPropsType<typeof getStaticProps>
  selected: string;
}

export type PokemonCardProps = {
  pokemonDetail: Pokemon
}

export type LoginProps = {
  email: string
  password: string
}
