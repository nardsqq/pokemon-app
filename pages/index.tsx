import Head from 'next/head'
import { NextPage } from 'next'
import { Flex, Center, Stack } from '@chakra-ui/react'
import LoginForm from '../components/auth/LoginForm'

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Luxor Technical Challenge</title>
        <meta name="description" content="Luxor Technical Challenge - Pokemon Dashboard" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Flex justify="center" align="center">
        <Center w="100%" h="100vh">
          <Stack spacing="36px" alignItems="center">
            <LoginForm />
          </Stack>
        </Center>
      </Flex>
    </>
  )
}

export default Home
