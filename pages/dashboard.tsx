import { NextPage } from 'next'
import { Flex, Center } from '@chakra-ui/react'
import { GetStaticProps, InferGetStaticPropsType } from 'next'
import { gql } from '@apollo/client'

import client from './api/client'
import PokemonPanel from '../components/dashboard/PokemonPanel'


// There are only 151 pokemons from the API in total
const GET_POKEMONS_QUERY = gql`
  query getPokemons {
    pokemons(first: 150) {
      id
      number
      name
      image
      classification
      types
      maxCP
      maxHP
      resistant
      weaknesses
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
    }
  }    
`

const Dashboard: NextPage = ({ pokemons }: InferGetStaticPropsType<typeof getStaticProps>) => {
  return (
    <Flex justifyContent="center">
      <Center height="100vh">
        <PokemonPanel pokemons={pokemons} />
      </Center>
    </Flex>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const { data } = await client.query({
    query: GET_POKEMONS_QUERY
  })

  return {
    props: {
      pokemons: data.pokemons
    }
  }
}

export default Dashboard
